# Actualizar *sample_data* de objeto *phyloseq* con nuevo archivo
---

El archivo `ksidon_18S_bigdata_01_ps_sampledata.tsv` se abre en Excel y se agregan los datos de interés.

Al guardar hay que asegurarse de que el formato sea `tsv`. Esto es, columnas separadas por TABs.

Para el primer ejemplo el nombre ahora es `ksidon_18S_bigdata_01_ps_sampledata_2.tsv`.

Para el segundo ejemplo supongamos que en lugar de `tsv`, el archivo se guardó como `csv` (columnas separadas por comas).

En R. El directorio de trabajo contiene los archivos que se requieren.

Cargar paquete y objetos `phyloseq` 
```R
library(phyloseq)
load("ksidon_18S_bigdata_01_ps.RData")

ls()
#[1] "ps_pr2"   "ps_silva"
```

## Ejemplo con archivo **tsv**

Verificar objeto phyloseq
```R	
ps_pr2
# phyloseq-class experiment-level object
# otu_table()   OTU Table:         [ 15614 taxa and 191 samples ]
# sample_data() Sample Data:       [ 191 samples by 6 sample variables ]
# tax_table()   Taxonomy Table:    [ 15614 taxa by 8 taxonomic ranks ]
# refseq()      DNAStringSet:      [ 15614 reference sequences ]
```
La tabla de metadatos tiene sólo 6 columnas

Verificar tabla de metadatos
```R
head(sample_data(ps_pr2))
# Sample Data:        [6 samples by 6 sample variables]:
#                          SampleID Estacion Profundidad Crucero Repeticion
# A10SW08DCMX5as39 A10SW08DCMX5as39      A10         DCM      X5          a
# A10SW08DCMX5bs39 A10SW08DCMX5bs39      A10         DCM      X5          b
# A10SW08DCMX6as39 A10SW08DCMX6as39      A10         DCM      X6          a
# A10SW08DCMX6bs39 A10SW08DCMX6bs39      A10         DCM      X6          b
# A10SW08SURX5as39 A10SW08SURX5as39      A10         SUR      X5          a
# A10SW08SURX5bs39 A10SW08SURX5bs39      A10         SUR      X5          b
#                 Secuenciacion
# A10SW08DCMX5as39            39
# A10SW08DCMX5bs39            39
# A10SW08DCMX6as39            39
# A10SW08DCMX6bs39            39
# A10SW08SURX5as39            39
# A10SW08SURX5bs39            39
```

Cargar la nueva tabla de metadatos (`tsv`)
```R
samtsv <- read.table("ksidon_18S_bigdata_01_sampledata_2.tsv", header=T, stringsAsFactors=F)
dim(samtsv)
# [1] 191   7
```
Poner el ID de las muestras como nombres de filas
```R
rownames(samtsv) <- samtsv$SampleID
```
Reemplazar tabla de datos en objeto phyloseq
```R
sample_data(ps_pr2) <- samtsv
```
Verificar el objeto
```R
ps_pr2
# phyloseq-class experiment-level object
# otu_table()   OTU Table:         [ 15614 taxa and 191 samples ]
# sample_data() Sample Data:       [ 191 samples by 7 sample variables ]
# tax_table()   Taxonomy Table:    [ 15614 taxa by 8 taxonomic ranks ]
# refseq()      DNAStringSet:      [ 15614 reference sequences ]
```
El número de cols ahora es 7

Verificar la actualización de metadatos
```R
head(sample_data(ps_pr2))
```

## Ejemplo con un archivo **csv**

Verificar objeto phyloseq (con asignación de silva)
```R
ps_silva
# phyloseq-class experiment-level object
# otu_table()   OTU Table:         [ 15614 taxa and 191 samples ]
# sample_data() Sample Data:       [ 191 samples by 6 sample variables ]
# tax_table()   Taxonomy Table:    [ 15614 taxa by 8 taxonomic ranks ]
# refseq()      DNAStringSet:      [ 15614 reference sequences ]
```
La tabla de metadatos tiene sólo 6 columnas

Cargar nuevos metadatos (`csv`)
```R
samcsv <- read.csv("ksidon_18S_bigdata_01_sampledata_2.csv")
dim(samcsv)
# [1] 191	7
```
Poner el ID de las muestras como nombres de filas
```R
rownames(samcsv) <- samcsv$SampleID
```
Reemplazar tabla de datos en objeto phyloseq
```R
sample_data(ps_silva) <- samcsv
```
Verificar el objeto modificado
```R
ps_silva
	# phyloseq-class experiment-level object
	# otu_table()   OTU Table:         [ 15614 taxa and 191 samples ]
	# sample_data() Sample Data:       [ 191 samples by 7 sample variables ]
	# tax_table()   Taxonomy Table:    [ 15614 taxa by 8 taxonomic ranks ]
	# refseq()      DNAStringSet:      [ 15614 reference sequences ]
```
El número de cols ahora es 7

Verificar los metadatos
```R
head(sample_data(ps_silva))
```

Ahora se puede continuar con el uso de los objetos `phyloseq`.